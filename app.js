// function corX(name="mustafa",surname="çor"){ // ES6 Öncesi class (constructor/(prototype))
//     this.name = name;
//     this.surname = surname;
//     this.tanit = ()=>{
//         corX.prototype.test = function(){
//             console.log("dada");
//         }
//     }
// }
// function deryax(name="mustafa",surname="çor"){ // ES6 Öncesi class (constructor/(prototype))
//     this.name = name;
//     this.surname = surname;
//     this.tanit = ()=>{
//         corX.prototype.test = function(){
//             console.log("dada");
//         }
//     }
// }
// deryax.prototype = Object.create(corX);
// let a = new corX("da","dada");


// console.log(deryax.prototype.__proto__);
// console.log(corX.prototype.__proto__);
// const obj1 = {
//     number1:20,
//     number2:30
// }
// const obj2 = {
//     number1:30,
//     number2:50
// }
// function addNumbers(number3,number4){
//     console.log(this.number1+this.number2+number3+number4);
// }
// addNumbers.call(obj1,100,200);
// addNumbers.apply(obj2,[100,200]);
// const copyFunc = addNumbers.bind(obj1);
// copyFunc(25,25);
//---------- Kalıtım ES6 Öncesi ----------//
// function Person(name,age){
//     this.name = name;
//     this.age = age;

// }
// Person.prototype.showInfos = function(){
//     console.log(this.name + " " + this.age);
// }
// function Employee(name,age,salary){
//     this.name = name;
//     this.age = age;
//     this.salary = salary;
// }
// Employee.prototype = Object.create(Person.prototype);
// Employee.prototype.toString = function(){
//     console.log("Employee "+this.name);
// }
// Employee.prototype.showInfos = function(){
//     console.log(this.name,this.age,this.salary);
// }
// function maasZam(zam){
//     console.log(`${this.salary}'liralık maaşın %${zam} zamlı hali =`+(this.salary + (this.salary*(zam/100))));
// }
// const kisi = new Person("Mustafa",19);
// const emp = new Employee("Mustafa",25,7000);
// maasZam.call(emp,50);
// maasZam.apply(emp,[50])
// const copyMaasZam = maasZam.bind(emp);
// copyMaasZam(60);
// emp.toString();
// emp.showInfos();
// --------- ES6 OOP -------- //

// class İşçi {
//     constructor(name, age, salary) {
//         this.name = name;
//         this.age = age;
//         this.salary = salary;
//     }
//     showInfos(){
//         console.log(this.name,this.age,this.salary);
//     }
// }
// let corx = new İşçi("Mustafa",20,9000);
// corx.showInfos()
// console.log(corx.name,corx.age,corx.salary);
// class Matematik{
//     static cube(x){
//         return x*x*x;
//     }
// }
// console.log(Matematik.cube(5));
// class Person{ //SuperClass,BaseClass
//     constructor(name,age) {
//         this.name = name;
//         this.age = age;
//     }
//     showInfos(){
//         console.log(this.name,this.age);
//     }
// }
// class Employee extends Person{ //DerivedClass,Subclass,ChildClass
//     constructor(name,age,salary){
//         // this.name = name;
//         // this.age = age;
// //        super(name,age);
//         super(name,age).showInfos();
//         this.salary = salary;
//     }
// }
// const corX = new Employee("crx",20,7000);
//console.log(corX.name,corX.age,corX.salary);
const form = document.getElementById("film-form");
const titleElement = document.querySelector("#title");
const directorElement = document.querySelector("#director");
const urlElement = document.querySelector("#url");
const cardBody = document.querySelectorAll(".card-body")[1];
const clear = document.querySelector("#clear-films");
//UI OBJESİNİ BAŞLATMA
const ui = new UI();
//Storage Nesnesi
const storage = new Storages();
//TÜM EVENTLERİ BAŞLATMA
eventListeners();
function eventListeners(){
    clear.addEventListener("click",clearAllFilms);
    cardBody.addEventListener("click",deleteFilm);
    document.addEventListener("DOMContentLoaded",()=>{
        let films = storage.getFilmsFromStorage();
        ui.loadAllFilms(films);
    })
    form.addEventListener("submit",addFilm);
};
function deleteFilm(e){
    if(e.target.id == "delete-film"){
        ui.deleteFilmFromUI(e.target);
        storage.deleteFilmFromStorage(e.target.parentElement.previousElementSibling.previousElementSibling.textContent);
    }
}
function addFilm(e){
    e.preventDefault();
    const title = titleElement.value;
    const director = directorElement.value;
    const url = urlElement.value;
    if (title == "" || url == "" || director == "") {
        //Display Error Message from using ui class methods
        ui.displayMessage("Boş alan bırakmadan ekleyin!","danger");
    }else{
        const newFilm = new Film(title,director,url);
        storage.addFilmToStorage(newFilm);
        ui.addFilmToUI(newFilm); //Arayüze Film ekleme
        ui.clearInputs(titleElement,urlElement,directorElement);
        ui.displayMessage("Film Başarı ile eklendi","success")
    }
   
}
function clearAllFilms(){
    ui.clearAllFilmsFromUI();
    storage.clearAllFilmsFromStorage();
    ui.displayMessage("Tüm filmler Silindi..","warning");
}